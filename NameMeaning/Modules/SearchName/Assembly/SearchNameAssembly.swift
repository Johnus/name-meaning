//
//  SearchNameSearchNameAssembly.swift
//  NameMeaning
//
//  Created by Yevgeniy Karnakov on 04/09/2019.
//  Copyright © 2019 ClearCode. All rights reserved.
//

import Swinject
import SwinjectStoryboard

class SearchNameAssemblyContainer: Assembly {

    func assemble(container: Container) {

        container.register(SearchNameInteractor.self) { (_, presenter: SearchNamePresenter) in
            let interactor = SearchNameInteractor()
            interactor.output = presenter
            return interactor
        }

        container.register(SearchNameRouter.self) { (_, viewController: SearchNameViewController) in
            let router = SearchNameRouter()
            router.transitionHandler = viewController
            return router
        }

        container.register(SearchNamePresenter.self) { (resolver, viewController: SearchNameViewController) in
            let presenter = SearchNamePresenter()
            presenter.view = viewController
            presenter.interactor = resolver.resolve(SearchNameInteractor.self, argument: presenter)
            presenter.router = resolver.resolve(SearchNameRouter.self, argument: viewController)
            return presenter
        }

        container.storyboardInitCompleted(SearchNameViewController.self) { resolver, viewController in
            viewController.output = resolver.resolve(SearchNamePresenter.self, argument: viewController)
        }
    }

}
