//
//  SearchNameSearchNamePresenter.swift
//  NameMeaning
//
//  Created by Yevgeniy Karnakov on 04/09/2019.
//  Copyright © 2019 ClearCode. All rights reserved.
//
import RealmSwift
import RxSwift
import RxCocoa

class SearchNamePresenter: ISearchNameModuleInput {

    // MARK: - Public Variables
    
    weak var view: ISearchNameViewInput!
    var interactor: ISearchNameInteractorInput!
    var router: ISearchNameRouterInput!
    var viewModels: [NMNameViewModel] = []

    // MARK: - Public Functions

    func viewIsReady() {
        let viewModel = createModel(query: "А", favorite: false)
        view.update(with: viewModel)
    }
    
    func randomName() {
        self.viewModels.removeAll()
        let realm = try? Realm()
        var result = realm!.objects(NameRussian.self)
        let randomName = result.randomElement()
        search(query: randomName?.name ?? "", favorite: false)
    }

    // MARK: - Private Functions
    
    private func createModel(query: String, favorite: Bool) -> NMNameListViewModel{
        self.viewModels.removeAll()
        let realm = try? Realm()
        var searchAttribute = favorite ? "favorite = true" : "name contains '\(query)'"
        if (!query.isEmpty) && (favorite) {
           searchAttribute = "favorite = true AND name contains '\(query)'"
        }
        var result = realm!.objects(NameRussian.self).filter(searchAttribute)
        
        if (query.isEmpty) && (!favorite) {
            result = realm!.objects(NameRussian.self)
        }
        // заполням модель отфильтрованных имен
        for item in result {
            let viewModel = NMNameViewModel(name: item.name,
                                            descriptionName: item.descriptionName,
                                            isFavorite: item.favorite)
            viewModel.output.onFavoriteChanged.asObservable()
                .subscribe(onNext: { (value) in
                    try? realm?.write {
                        item.favorite = value
                    }
                   self.search(query: query, favorite: false)
                })
            self.viewModels.append(viewModel)
        }
        let listViewModel = NMNameListViewModel(name: self.viewModels)
        return listViewModel
    }
}

    // MARK: - Extensions

extension SearchNamePresenter: ISearchNameViewOutput {
    func search(query: String?, favorite: Bool) {
        let viewModel = createModel(query: query ?? "", favorite: favorite)
        view.update(with: viewModel)
    }
}

extension SearchNamePresenter: ISearchNameInteractorOutput {
    
    func onError(_ error: Error?) {
        router.showAlert(withMessage: error?.localizedDescription ?? "Не известная ошибка")
    }
    
    func onComplete() {
        router.hideLoading()
    }
}
