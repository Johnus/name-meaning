//
//  SearchNameSearchNameInteractorOutput.swift
//  NameMeaning
//
//  Created by Yevgeniy Karnakov on 04/09/2019.
//  Copyright © 2019 ClearCode. All rights reserved.
//

import Foundation

protocol ISearchNameInteractorOutput: class {

    func onError(_ error: Error?)
    func onComplete()
    
}
