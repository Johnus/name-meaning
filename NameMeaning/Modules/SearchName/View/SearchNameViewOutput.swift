//
//  SearchNameSearchNameViewOutput.swift
//  NameMeaning
//
//  Created by Yevgeniy Karnakov on 04/09/2019.
//  Copyright © 2019 ClearCode. All rights reserved.
//

protocol ISearchNameViewOutput {

    /**
        @author Yevgeniy Karnakov
        Notify presenter that view is ready
    */

    func viewIsReady()
    func search(query: String?, favorite: Bool)
    func randomName()
}
