//
//  SearchNameSearchNameViewInput.swift
//  NameMeaning
//
//  Created by Yevgeniy Karnakov on 04/09/2019.
//  Copyright © 2019 ClearCode. All rights reserved.
//

protocol ISearchNameViewInput: class {

    func update(with viewModels: NMNameListViewModel)
    func isFavorite(_ isFavorite: Bool)
    func setupInitialState()
}
