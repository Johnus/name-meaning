//
//  SearchNameSearchNameViewController.swift
//  NameMeaning
//
//  Created by Yevgeniy Karnakov on 04/09/2019.
//  Copyright © 2019 ClearCode. All rights reserved.
//

import UIKit
import TableKit
import SnapKit

class SearchNameViewController: UIViewController {
    // MARK: - IBOutlets


    @IBOutlet weak var nameSearchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableKit = TableDirector(tableView: tableView)
        }
    }
    
    // MARK: - Private Variables
    
    private var favorite: Bool = false
    
    // MARK: - Variables
    
    private var tableKit: TableDirector!
    var output: ISearchNameViewOutput!
    
    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationMenuButtons()
        output.viewIsReady()
        setupConstraints()
        setupView()
        self.hideKeyboardWhenTappedAround() 
    }

    // MARK: - Private functions
    
    private func setupView() {
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.NMDarkLight
    }
    
    private func setupConstraints() {

    }
    
    private func navigationMenuButtons() {
        let favorite = UIBarButtonItem(image: R.image.star(),
                                       style: .plain,
                                       target: self, action: #selector(searchFavorite))
        let randomButton = UIBarButtonItem(image: R.image.random(),
                                           style: .plain,
                                           target: self,
                                           action: #selector(randomName))
        self.navigationItem.title = "Значение имени"
        self.navigationItem.rightBarButtonItem = favorite
        self.navigationItem.leftBarButtonItem = randomButton
    }
    
    // MARK: - OBJC Functions
    
    @objc private func searchFavorite() {
        favorite.toggle()
        nameSearchBar.text = ""
        output.search(query: "", favorite: favorite)
        isFavorite(favorite)
    }
    
    @objc private func randomName() {
        output.randomName()
    }
    
    // MARK: - Extensions

}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension SearchNameViewController: ISearchNameViewInput {

    func setupInitialState() {
    }

    func update(with viewModels: NMNameListViewModel) {
        tableKit.clear()
        let section = TableSection(rows: [])
        section.headerHeight = 0.1
        viewModels.name.forEach { (cellViewModel) in
            let row = TableRow<NMNameTableViewCell>(item: cellViewModel)
            section.append(row: row)
        }
        tableKit.append(section: section)
        tableKit.reload()
    }
    
    func isFavorite(_ isFavorite: Bool) {
        self.navigationItem.rightBarButtonItem?.tintColor = isFavorite ? UIColor.NMBlue : UIColor.NMDarkLight
    }
    
}

extension SearchNameViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        output.search(query: searchBar.text ?? "", favorite: favorite)
        print(searchBar.text)
    }
}
