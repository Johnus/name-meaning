//
//  Alertable.swift
//  NameMeaning
//
//  Created by Yevgeniy Karnakov on 29.08.2019.
//  Copyright © 2019 Yevgeniy Karnakov. All rights reserved.
//

import Foundation
import UIKit

protocol Alertable: Routable {
    
}

extension Alertable {
    
    func showAlert(withMessage message: String, title: String? = nil, closeHandler: (() -> Void)? = nil) {
        guard let transitionHandler = transitionHandler as? UIViewController else {
            debugPrint("Transition handler should be UIViewController.")
            return
        }
        transitionHandler.showAlert(withMessage: message, title: title, closeHandler: closeHandler)
    }
}
