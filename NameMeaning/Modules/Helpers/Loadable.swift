//
//  Loadable.swift
//  NameMeaning
//
//  Created by Yevgeniy Karnakov on 29.08.2019.
//  Copyright © 2019 Yevgeniy Karnakov. All rights reserved.
//

import Foundation
import LightRoute
import SVProgressHUD

protocol Loadable: Routable {
    
}

extension Loadable {
    
    func showLoading() {
        SVProgressHUD.show()
    }
    
    func hideLoading() {
        SVProgressHUD.dismiss()
    }
    
}
