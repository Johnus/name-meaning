//
//  ViewController.swift
//  NameMeaning
//
//  Created by Yevgeniy Karnakov on 29.08.2019.
//  Copyright © 2019 Yevgeniy Karnakov. All rights reserved.
//

import UIKit
import SnapKit
import RealmSwift

class ViewController: UIViewController {
    
    // MARK: - Lazy Variables
    private lazy var searchButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Search...", for: .normal)
        button.addTarget(self, action: #selector(searchName), for: .touchUpInside)
        view.addSubview(button)
        return button
    }()
    
    private lazy var searchTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Search"
        textField.layer.borderColor = UIColor.init(red: 0, green: 0, blue: 254, alpha: 0.3).cgColor
        textField.layer.borderWidth = 1
        view.addSubview(textField)
        return textField
    }()
    
    // MARK: - Private variables
    
    // MARK: - Life Circle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
    }
    
    // MARK: - Private Functions
    
    private func setupView() {}
    private func setupConstraints() {
        searchTextField.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(100)
            make.left.right.equalToSuperview().inset(50)
        }
        searchButton.snp.makeConstraints { (make) in
            make.top.equalTo(searchTextField.snp.bottom).offset(10)
            make.centerX.equalToSuperview()
        }
    }
    
    private func setupRealm() {

    }
    
    @objc private func searchName() {
        print("Searching...")
       // let nameOfRussian = NameRussian()
        let config = Realm.Configuration(
            // Get the URL to the bundled file
            fileURL: Bundle.main.url(forResource: "russian", withExtension: "realm"),
            // Open the file in read-only mode as application bundles are not writeable
            readOnly: true)
        let realm = try? Realm(configuration: config)
        print(config)
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        let result = realm!.objects(NameRussian.self).filter("name contains '\("Мар")'")
        print(result.count)
        for item in result {
            print(item.name.description)
        }
       // print(result.first?.description)
    }

}
