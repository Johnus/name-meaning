//
//  nameOfRussian.swift
//  NameMeaning
//
//  Created by Yevgeniy Karnakov on 02.09.2019.
//  Copyright © 2019 Yevgeniy Karnakov. All rights reserved.
//

import Foundation
import RealmSwift

class NameRussian: Object {
    @objc dynamic var name = ""
    @objc dynamic var descriptionName = ""
    @objc dynamic var sex = 0
    @objc dynamic var favorite = false
}
