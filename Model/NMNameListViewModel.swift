//
//  NMNameListViewModel.swift
//  NameMeaning
//
//  Created by Yevgeniy Karnakov on 06.09.2019.
//  Copyright © 2019 Yevgeniy Karnakov. All rights reserved.
//

import Foundation

class NMNameListViewModel {
    let name: [NMNameViewModel]
    
    init(name: [NMNameViewModel]) {
        self.name = name
    }
}
