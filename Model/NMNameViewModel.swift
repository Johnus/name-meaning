//
//  NMNameViewModel.swift
//  NameMeaning
//
//  Created by Yevgeniy Karnakov on 06.09.2019.
//  Copyright © 2019 Yevgeniy Karnakov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct NMNameViewModel {
    
    let input: NMNameViewModel.Input
    let output: NMNameViewModel.Output
    
    struct Input {
        let isFavorite: BehaviorSubject<Bool>
    }
    struct Output {
        let onFavoriteChanged: Driver<Bool>
    }
    
    let name: String?
    let descriptionName: String?
    
    init(name: String, descriptionName: String, isFavorite: Bool) {
        self.name = name
        self.descriptionName = descriptionName
        self.input = Input(isFavorite: BehaviorSubject<Bool>(value: isFavorite))
        self.output = Output(onFavoriteChanged: input.isFavorite.asDriver(onErrorJustReturn: false).skip(1))//подписываемся на
                                                                //изменение в Input пропускаем первый (значение до подписка - дефолтное в данном случае)
                                                                // - нужен только значение после подписки
        
    }
}
