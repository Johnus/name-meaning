//
//  UIColor+Ext.swift
//  NameMeaning
//
//  Created by Yevgeniy Karnakov on 06.09.2019.
//  Copyright © 2019 Yevgeniy Karnakov. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    static var NMBlue = #colorLiteral(red: 0.26, green: 0.63, blue: 0.85, alpha: 1)
    static var NMDark = #colorLiteral(red: 0.41, green: 0.41, blue: 0.41, alpha: 1)
    static var NMDarkLight = #colorLiteral(red: 0.67, green: 0.67, blue: 0.67, alpha: 1)
    static var NMGrayLight = #colorLiteral(red: 0.88, green: 0.88, blue: 0.88, alpha: 1)
    static var NMBlack = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.87)
    static var NMRed = #colorLiteral(red: 0.8, green: 0.11, blue: 0.38, alpha: 1)
    
}
