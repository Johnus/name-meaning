//
//  Font+Ext.swift
//  NameMeaning
//
//  Created by Yevgeniy Karnakov on 06.09.2019.
//  Copyright © 2019 Yevgeniy Karnakov. All rights reserved.
//

import Foundation
import UIKit

fileprivate extension UIFont {
    
    static func lightFont(ofSize size: CGFloat) -> UIFont {
        return R.font.robotoMedium(size: size)!
    }
    
    static func regularFont(ofSize size: CGFloat) -> UIFont {
        return R.font.robotoMedium(size: size)!
    }
    
    static func boldFont(ofSize size: CGFloat) -> UIFont {
        return R.font.robotoMedium(size: size)!
    }
    
    static func mediumFont(ofSize size: CGFloat) -> UIFont {
        return R.font.robotoMedium(size: size)!
    }
    
}
extension UIFont {
    
    static var windowMessage: UIFont {
        return .regularFont(ofSize: 18)
    }
    
    static var lightWindowMessage: UIFont {
        return .regularFont(ofSize: 16)
    }
    
    static var titleField: UIFont {
        return .lightFont(ofSize: 18)
    }
    
    static var valueField: UIFont {
        return .regularFont(ofSize: 18)
    }
    
    static var titleButton: UIFont {
        return .mediumFont(ofSize: 18)
    }
    
}
