//
//  UIStoryboard+Ext.swift
//  MeetLook
//
//  Created by Dmitriy Safarov on 04/07/2019.
//  Copyright © 2019 SimpleCode. All rights reserved.
//

import Foundation
import LightRoute

extension UIStoryboard {
    
    var factory: StoryboardFactory {
        return StoryboardFactory(storyboard: self)
    }
    
}
