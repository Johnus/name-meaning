//
//  NMNameTableViewCell.swift
//  NameMeaning
//
//  Created by Yevgeniy Karnakov on 06.09.2019.
//  Copyright © 2019 Yevgeniy Karnakov. All rights reserved.
//

import Foundation
import TableKit
import SnapKit
import RxSwift
import RxCocoa

class NMNameTableViewCell: UITableViewCell, ConfigurableCell {
    
    // MARK: - Lazy Variables
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = R.font.robotoRegular(size: 18)
        label.textColor = UIColor.NMBlack
        label.text = "Name"
        label.numberOfLines = 0
        contentView.addSubview(label)
        return label
    }()
    
    lazy var descriptionName: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = UIColor.NMDark
        label.font = R.font.robotoLight(size: 14)
        label.text = "Description"
        contentView.addSubview(label)
        return label
    }()
    
    lazy var favoritesButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(R.image.star(), for: .normal)
        button.tintColor = UIColor.NMDarkLight
        button.clipsToBounds = true
        contentView.addSubview(button)
        return button
    }()
    
    // Private variables
    
    private var tintColorButton: UIColor = UIColor.NMBlue
    private var disposeBag = DisposeBag()
    
    // MARK: - Public Variables
    
    public var isFavorite: Bool = false {
        didSet {
            self.tintColorButton = isFavorite ? UIColor.NMBlue : UIColor.NMDarkLight
            favoritesButton.tintColor = tintColorButton
        }
    }
    
    // MARK: - Overide Function
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
        setupConstraints()
    }
    
    // MARK: - Private Functions
    
    private func setupViews() {}
    
    private func setupConstraints() {
        
        nameLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(4)
            make.left.equalToSuperview().offset(4)
            make.right.equalTo(favoritesButton.snp.left).offset(-4)
        }
        
        descriptionName.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(4)
            make.right.equalTo(favoritesButton.snp.left).offset(-4)
            make.top.equalTo(nameLabel.snp.bottom).offset(4)
            make.bottom.equalToSuperview().offset(-4)
        }
        
        favoritesButton.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-4)
            make.centerY.equalToSuperview()
            make.height.equalTo(50)
            make.width.equalTo(50)
        }
    }
    
    private func setupBindings(viewModel: NMNameViewModel) {
        // извлекаем начальное значение Избранное
        if let isFavorite = try? viewModel.input.isFavorite.value() {
            self.isFavorite = isFavorite
        }
        favoritesButton.rx.tap.bind { // подписываемся на клик
            self.isFavorite.toggle() // инверсия значения
            viewModel.input.isFavorite.onNext(self.isFavorite) // эмитим(отправляем) новое значение
        }
        .disposed(by: disposeBag)
        
    }
    
    override func prepareForReuse() { // обнуляем содержимое ячейки. Необходимо в связи с тем что ячейки не исчезают а переиспользуются
        super.prepareForReuse()
        disposeBag = DisposeBag()
        nameLabel.text = ""
        descriptionName.text = ""
    }
    
    // MARK: - Public Functions
    
    func configure(with data: NMNameViewModel) {
        disposeBag = DisposeBag()
        nameLabel.text = data.name
        descriptionName.text = data.descriptionName
        setupBindings(viewModel: data)
    }
    
}
